package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	String var1 = "Hello";
	String var2 = "a";
	String var3 = "b";
	String var4 = "c";
	String var5 = "ert";
	String var6 = "ytmlop";
	String var7 = "zrnv";
	String var8 = "hfç_(";
	String var9 = "hfycj";
	String var10 = "hfyff";


	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/home")
	String sayHello(){
		return "Hello guys";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
